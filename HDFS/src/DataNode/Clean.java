package DataNode;

import java.lang.*;
import java.util.*;
import java.io.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class Clean {

    private Clean() throws IOException {

        try {
            Properties prop = new Properties();
            InputStream input = null;
            String filename = "config.datanode";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            prop.load(input);
            int RMIPORT = Integer.parseInt(prop.getProperty("rmi_port"));
            LocateRegistry.getRegistry(RMIPORT).unbind("DataNode");
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
    }
}

