package DataNode;

import java.util.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.lang.*;
import java.io.*;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.TimerTask;
import java.util.Timer;
import java.io.File;
import java.io.FilenameFilter;
import java.net.Inet4Address;
import NameNode.INameNode;
import com.distributedsystems.HDFS.*;
import com.google.protobuf.ByteString;

public class DataNode implements IDataNode {

    private static int RMIPORT;
    // 32MB is the block size
    private static int BLOCK_SIZE;

    public static int datanode_id;
    private static String namenode_ip;
    private static int namenode_port;
    private static String datanode_ip;
    private static int HeartBeatDelay;
    private static int BlockReportDelay;
    private static Registry registry;
    private static IDataNode exported_stub;
    private static INameNode namenode_stub;
    private static HeartBeatThread heartBeatThread;
    private static BlockReportThread blockReportThread;

    private static Vector my_blocks;

    private File[] finder() {
        // current directory
        File dir = new File(".");
        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".block");
            }
        });

    }

    public DataNode(int id) {

        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "hdfs.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }

            prop.load(input);
            this.datanode_id = id;
            this.namenode_ip = prop.getProperty("namenode_ip");
            this.namenode_port = Integer.parseInt(prop.getProperty("namenode_port"));
            this.HeartBeatDelay = Integer.parseInt(prop.getProperty("heart_beat_delay"));
            this.BlockReportDelay = Integer.parseInt(prop.getProperty("block_report_delay"));
            this.RMIPORT = Integer.parseInt(prop.getProperty("datanode_port"));
            this.BLOCK_SIZE = Integer.parseInt(prop.getProperty("block_size"));
            this.my_blocks = new Vector(0);
            File[] block_files = this.finder();

            // add current block to datastructures incase of restart
            for (File i : block_files) {
                String[] arr = i.getName().toString().split("[\\.]");
                my_blocks.add(Integer.parseInt(arr[0]));
            }

        } catch (Exception e) {
            System.err.println("Error in constructor: " + e.toString());
            e.printStackTrace();
        }
    }

    public void start() {

        try {

            this.datanode_ip = Inet4Address.getLocalHost().getHostAddress();
            // export own registry and bind stub
            this.registry = LocateRegistry.createRegistry(this.RMIPORT);
            this.exported_stub = (IDataNode) UnicastRemoteObject.exportObject(this, 0);
            this.registry.bind("DataNode", exported_stub);

            // get NameNode stub
            Registry registry = LocateRegistry.getRegistry(this.namenode_ip, this.namenode_port);
            this.namenode_stub = (INameNode) registry.lookup("NameNode");

            // make the threads
            this.heartBeatThread = new HeartBeatThread(this.datanode_id, this.namenode_stub, this.HeartBeatDelay);
            this.blockReportThread = new BlockReportThread(this.namenode_stub, this.BlockReportDelay, this);

            // start the threads
            this.heartBeatThread.start();
            this.blockReportThread.start();

            System.err.println("DataNode Setup Complete.");
        } catch (Exception e) {
            System.err.println("Error in start: " + e.toString());
            e.printStackTrace();
        }
    }

    public Vector get_my_blocks() {
        // Vectors are thread-safe
        return (Vector) this.my_blocks.clone();
    }

    private int Cascade(WriteBlockRequest request) {
        try {
            if (request.getBlockInfo().getLocationsCount() == 0) {
                return 0;
            }
            Vector<DataNodeLocation> locations = new Vector<DataNodeLocation>();
            for (DataNodeLocation i : request.getBlockInfo().getLocationsList()) {
                locations.add(i);
            }

            // get stub for cascade request
            Registry registry = LocateRegistry.getRegistry(locations.get(0).getIp(), locations.get(0).getPort());
            IDataNode cascade_stub = (IDataNode) registry.lookup("DataNode");

            // removing location of node to which request is being sent
            locations.remove(0);
            BlockLocations.Builder location_builder = request.getBlockInfo().toBuilder();
            location_builder.clearLocations();
            location_builder.addAllLocations(locations);
            WriteBlockRequest.Builder request_builder = request.toBuilder();
            request_builder.clearBlockInfo().setBlockInfo(location_builder);

            // request cascade
            byte[] response = cascade_stub.writeBlock(request_builder.build().toByteArray());
            WriteBlockResponse write_resp = WriteBlockResponse.parseFrom(response);
            return write_resp.getStatus();
        } catch (Exception e) {
            System.err.println("Error in Cascade: " + e.toString());
            e.printStackTrace();
            return 2;
        }
    }

    /* ReadBlockResponse readBlock(ReadBlockRequest)) */
 /* Method to read data from any block given block-number */
    synchronized public byte[] readBlock(byte[] inp) throws RemoteException {
        ReadBlockResponse.Builder response = ReadBlockResponse.newBuilder();
        try {
            ReadBlockRequest request = ReadBlockRequest.parseFrom(inp);
            int requested_block = request.getBlockNumber();

            // Vector is thread-safe
            if (my_blocks.contains(requested_block)) {
                InputStream inputstream = new FileInputStream(requested_block + ".block");
                DataInputStream datainputstream = new DataInputStream(inputstream);
                // total_bytes will always be less than or equal to block size(since its reading from a block)
                int total_bytes = inputstream.available();
                byte[] read_bytes = new byte[total_bytes];
                // Reading total_bytes from <blocknum>.block. The file wont have more data than that.
                datainputstream.read(read_bytes);
                response.setStatus(0);
                response.setData(ByteString.copyFrom(read_bytes));
                inputstream.close();
            } else {
                response.setStatus(1);
            }
        } catch (Exception e) {
            System.err.println("Error in readBlock: " + e.toString());
            e.printStackTrace();
            response.setStatus(2);
        }
        return response.build().toByteArray();
    }

    /* WriteBlockResponse writeBlock(WriteBlockRequest) */
 /* Method to write data to a specific block */
    synchronized public byte[] writeBlock(byte[] inp) throws RemoteException {

        WriteBlockResponse.Builder response = WriteBlockResponse.newBuilder();
        try {
            WriteBlockRequest request = WriteBlockRequest.parseFrom(inp);
            File file_to_write = new File((request.getBlockInfo().getBlockNumber()) + ".block");
            if (file_to_write.exists() && !file_to_write.isDirectory()) {
                System.err.println("Warning: Block Already exists");
                response.setStatus(1);
            } else {
                // Open Stream in write mode
                FileOutputStream outstream = new FileOutputStream(file_to_write);
                outstream.write(request.getData().toByteArray());
                outstream.close();

                // Vectors are thread-safe
                my_blocks.add(request.getBlockInfo().getBlockNumber());
                int response_cascade = Cascade(request);
                response.setStatus(response_cascade);
            }
        } catch (Exception e) {
            System.err.println("Error in writeBlock: " + e.toString());
            e.printStackTrace();
            response.setStatus(2);
        }
        return response.build().toByteArray();
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Please provide DataNode id.");
            System.exit(1);
        }
        int firstArg = 0;
        try {
            firstArg = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.err.println("Argument" + args[0] + " must be an integer.");
            System.exit(1);
        }
        try {
            System.err.println("Setting up DataNode.");
            DataNode server = new DataNode(firstArg);
            server.start();

        } catch (Exception e) {
            System.err.println("Error Starting Server: " + e.toString());
            e.printStackTrace();
        }
    }

    static class BlockReportThread extends Thread {

        private INameNode stub_;
        private int BlockReportDelay;
        private DataNode dataNode_;

        public BlockReportThread(INameNode stub, int delay, DataNode dataNode) {
            this.stub_ = stub;
            this.BlockReportDelay = delay;
            this.dataNode_ = dataNode;
        }

        public void run() {
            try {
                while (true) {
                    BlockReportRequest.Builder blockreport_request = BlockReportRequest.newBuilder();
                    blockreport_request.setId(dataNode_.datanode_id);
                    DataNodeLocation.Builder datanode_loc = DataNodeLocation.newBuilder();
                    datanode_loc.setIp(this.dataNode_.datanode_ip);
                    datanode_loc.setPort(this.dataNode_.RMIPORT);
                    blockreport_request.setLocation(datanode_loc);
                    Vector<Integer> my_blocks = dataNode_.get_my_blocks();
                    blockreport_request.addAllBlockNumbers(my_blocks);
                    byte[] blockreport_response = this.stub_.blockReport(blockreport_request.build().toByteArray());
                    BlockReportResponse response = BlockReportResponse.parseFrom(blockreport_response);
                    int counter = 0;
                    for (int i : response.getStatusList()) {
                        if (i != 0) {
                            System.err.println("BlockNumber not found at NameNode: " + my_blocks.elementAt(counter));
                        }
                        counter++;
                    }

                    Thread.sleep(BlockReportDelay);
                }
            } catch (Exception e) {
                System.err.println("Error Running BlockReport Thread: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    static class HeartBeatThread extends Thread {

        private byte[] heartbeat_request;
        private INameNode stub_;
        private int HeartBeatDelay;

        public HeartBeatThread(int datanode_id, INameNode stub, int delay) {

            HeartBeatRequest.Builder request = HeartBeatRequest.newBuilder();
            request.setId(datanode_id);

            this.heartbeat_request = request.build().toByteArray();
            this.stub_ = stub;
            this.HeartBeatDelay = delay;
        }

        public void run() {
            try {
                while (true) {
                    byte[] heartbeat_response = this.stub_.heartBeat(heartbeat_request);
                    HeartBeatResponse hbeat_resp = HeartBeatResponse.parseFrom(heartbeat_response);
                    if (hbeat_resp.getStatus() != 0) {
                        System.err.println("Error in HeartBeat Response");
                    }
                    Thread.sleep(HeartBeatDelay);
                }
            } catch (Exception e) {
                System.err.println("Error Running HeartBeat Thread: " + e.toString());
                e.printStackTrace();
            }
        }
    }
}
