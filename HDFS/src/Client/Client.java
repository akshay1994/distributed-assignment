package Client;

import DataNode.IDataNode;
import NameNode.INameNode;
import java.util.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.lang.*;
import java.io.*;
import java.nio.charset.Charset;
import com.distributedsystems.HDFS.*;
import com.distributedsystems.namenode.NameNodeProto.*;
import com.google.protobuf.ByteString;

public class Client {

    private static int BLOCK_SIZE;
    private static String namenode_ip;
    private static int namenode_port;
    private static Registry registry;
    private static INameNode namenode_stub;

    public Client() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "hdfs.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }

            prop.load(input);
            this.namenode_ip = prop.getProperty("namenode_ip");
            this.namenode_port = Integer.parseInt(prop.getProperty("namenode_port"));
            this.BLOCK_SIZE = Integer.parseInt(prop.getProperty("block_size"));
        } catch (Exception e) {
            System.err.println("Problem initialising Client " + e.getMessage());
            e.printStackTrace();
        }

    }

    private int put(String filename) {
        try {
            // Send write openfile request
            OpenFileRequest.Builder open_req = OpenFileRequest.newBuilder();
            open_req.setFileName(filename);
            open_req.setForRead(false);
            byte[] open_resp = this.namenode_stub.openFile(open_req.build().toByteArray());
            OpenFileResponse response = OpenFileResponse.parseFrom(open_resp);

            if (response.getStatus() != 0) {
                System.err.println("Error in response when openfile request for writing was sent.");
                return 1;
            } else {
                int handle = response.getHandle();
                // No need to rebuild since same handle is used for consecutive calls
                AssignBlockRequest.Builder assign_blk_req = AssignBlockRequest.newBuilder();
                assign_blk_req.setHandle(handle);
                byte[] assign_req = assign_blk_req.build().toByteArray();

                InputStream inputstream = new FileInputStream(filename);
                DataInputStream datainputstream = new DataInputStream(inputstream);
                int total_bytes = inputstream.available();
                byte[] read_bytes;
                while (total_bytes > 0) {
                    if (total_bytes > this.BLOCK_SIZE) {
                        read_bytes = new byte[this.BLOCK_SIZE];
                        total_bytes = total_bytes - this.BLOCK_SIZE;
                    } else {
                        read_bytes = new byte[total_bytes];
                        total_bytes = 0;
                    }
                    datainputstream.read(read_bytes);
                    // read file and for each 32 MB block do
                    // Assign block
                    byte[] assign_response = this.namenode_stub.assignBlock(assign_req);
                    AssignBlockResponse ass_resp = AssignBlockResponse.parseFrom(assign_response);
                    if (ass_resp.getStatus() != 0) {
                        System.err.println("Error in response from Assign Block request.");
                        return 1;
                    } else {
                        // get the assigned block
                        BlockLocations assigned_block = ass_resp.getNewBlock();
                        // send write request to datanode with the assigned block
                        int block_number = assigned_block.getBlockNumber();
                        if (assigned_block.getLocationsCount() == 0) {
                            System.err.println("No data node location to write block");
                            return 1;
                        } else {
                            String ip = assigned_block.getLocations(0).getIp();
                            int port = assigned_block.getLocations(0).getPort();
                            Vector<DataNodeLocation> ass_blk_list = new Vector<DataNodeLocation>();
                            for (DataNodeLocation i : assigned_block.getLocationsList()) {
                                ass_blk_list.add(i);
                            }
                            ass_blk_list.remove(0);
                            BlockLocations.Builder updated_ass_blk = BlockLocations.newBuilder();
                            // removed block location to which request is being sent to prevent rewriting during cascade phase
                            updated_ass_blk.setBlockNumber(block_number);
                            updated_ass_blk.addAllLocations(ass_blk_list);
                            Registry datanode_registry = LocateRegistry.getRegistry(ip, port);
                            IDataNode datanode_stub = (IDataNode) datanode_registry.lookup("DataNode");
                            WriteBlockRequest.Builder write_block_req = WriteBlockRequest.newBuilder();
                            write_block_req.setData(ByteString.copyFrom(read_bytes));
                            write_block_req.setBlockInfo(updated_ass_blk);

                            byte[] write_resp = datanode_stub.writeBlock(write_block_req.build().toByteArray());
                            WriteBlockResponse write_response = WriteBlockResponse.parseFrom(write_resp);
                            if (write_response.getStatus() != 0) {
                                System.err.println("Error in WriteBlockResponse");
                                return 1;
                            } else {
                                System.err.println("Block written successfully");
                            }
                        }

                    }
                }
                inputstream.close();
                CloseFileRequest.Builder close_file_req = CloseFileRequest.newBuilder();
                close_file_req.setHandle(handle);
                byte[] close_file_resp = this.namenode_stub.closeFile(close_file_req.build().toByteArray());
                CloseFileResponse close_response = CloseFileResponse.parseFrom(close_file_resp);
                if (close_response.getStatus() != 0) {
                    System.err.println("Error in Close File response");
                    return 1;
                } else {
                    System.err.println("Close File response Completed");
                    return 0;
                }
            }
        } catch (Exception e) {
            System.err.println("Problem running put. " + e.getMessage());
            e.printStackTrace();
            return 2;
        }
    }

    private int get(String filename) {
        try {
            OpenFileRequest.Builder open_req = OpenFileRequest.newBuilder();
            open_req.setFileName(filename);
            open_req.setForRead(true);
            byte[] open_resp = this.namenode_stub.openFile(open_req.build().toByteArray());

            OpenFileResponse response = OpenFileResponse.parseFrom(open_resp);

            if (response.getStatus() != 0) {
                System.err.println("Error in response when openfile request for reading was sent.");
                return 1;
            } else {
                int handle = response.getHandle();
                BlockLocationRequest.Builder blr = BlockLocationRequest.newBuilder();
                blr.addAllBlockNums(response.getBlockNumsList());
                byte[] blres = this.namenode_stub.getBlockLocations(blr.build().toByteArray());
                BlockLocationResponse block_loc_res = BlockLocationResponse.parseFrom(blres);
                if (block_loc_res.getStatus() != 0) {
                    System.err.println("Error in response when openfile request for reading was sent.");
                    return 1;
                } else {
                    File file_to_write = new File(filename);
                    if (file_to_write.exists() && !file_to_write.isDirectory()) {
                        System.err.println("Warning: File already exists, overwriting the current file");
                        file_to_write.delete();
                    }

                    // open outputstream in append mode
                    FileOutputStream outstream = new FileOutputStream(file_to_write, true);

                    for (BlockLocations j : block_loc_res.getBlockLocationsList()) {
                        int block_no = j.getBlockNumber();
                        List<DataNodeLocation> dnl = j.getLocationsList();
                        if (dnl.size() == 0) {
                            System.err.println("Error: No Location found for block number " + block_no);
                            return 1;
                        } else {
                            //TODO(arnav): Read from multiple Locations or just 1. Only 1 makes sense though. For now 1.
                            String ip = dnl.get(0).getIp();
                            int port = dnl.get(0).getPort();
                            Registry datanode_registry = LocateRegistry.getRegistry(ip, port);
                            IDataNode datanode_stub = (IDataNode) datanode_registry.lookup("DataNode");
                            ReadBlockRequest.Builder read_block_req = ReadBlockRequest.newBuilder();
                            read_block_req.setBlockNumber(block_no);
                            byte[] read_block_res = datanode_stub.readBlock(read_block_req.build().toByteArray());
                            ReadBlockResponse read_block_bres = ReadBlockResponse.parseFrom(read_block_res);

                            //write data to file
                            outstream.write(read_block_bres.getData().toByteArray());
                        }

                    }
                    outstream.close();

                    return 0;
                }
            }

        } catch (Exception e) {
            System.err.println("Problem running get. " + e.getMessage());
            e.printStackTrace();
            return 2;
        }
    }

    private int list() {
        try {
            byte[] request = this.namenode_stub.list(null);
            ListFilesResponse lfr = ListFilesResponse.parseFrom(request);
            if (lfr.getStatus() != 0) {
                System.err.println("Error in list response");
                return 1;
            }

            System.err.println("Printing List response");
            for (String i : lfr.getFileNamesList()) {
                System.out.println(i);
            }

            return 0;
        } catch (Exception e) {
            System.err.println("Problem running list. " + e.getMessage());
            e.printStackTrace();

            return 2;
        }
    }

    public static void main(String[] args) throws IOException {
        try {
            Client rpc_client = new Client();
            Registry registry = LocateRegistry.getRegistry(rpc_client.namenode_ip, rpc_client.namenode_port);
            rpc_client.namenode_stub = (INameNode) registry.lookup("NameNode");
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] tokens = line.split("[ \t]+");
                switch (tokens[0].toLowerCase()) {
                    case "put":
                        rpc_client.put(tokens[1]);
                        break;
                    case "get":
                        rpc_client.get(tokens[1]);
                        break;
                    case "list":
                        rpc_client.list();
                        break;
                }
            }

        } catch (Exception e) {
            System.err.println("Error Starting Server: " + e.toString());
            e.printStackTrace();
        }
    }

}
