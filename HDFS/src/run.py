#!/usr/bin/env python

import struct
import socket
import os
import signal
import sys

def ip2int(addr):                                                               
    return struct.unpack("!I", socket.inet_aton(addr))[0]                       

def int2ip(addr):                                                               
    return socket.inet_ntoa(struct.pack("!I", addr)) 

#TODO: Read from config file
NUM_DATANODES=5
NAMENODE_IP = "10.0.5.2"

DOCKER_IMAGE = "node_machine"
DOCKER_CMD = "docker run -d --net=\"my-net\" --ip="

INIT_CMD = "export PATH=$PATH:/usr/local/bin;eval $(docker-machine env docker);"
INIT_CMD += "docker network create --driver=bridge --subnet=10.0.5.0/24 my-net;"
INIT_CMD += "export CP=\"/distributed/libs/*:/distributed/:.\";"

CMD=INIT_CMD

#run namenode
CMD+=DOCKER_CMD + "\"" + NAMENODE_IP + "\" --name=NN " + DOCKER_IMAGE + ";"
CMD+="docker exec -d NN java -cp \"${CP}\" NameNode.NameNode;"

for i in range(1,NUM_DATANODES+1):
    datanode_ip = int2ip(ip2int(NAMENODE_IP)+i);
    CMD+=DOCKER_CMD + "\"" + datanode_ip + "\" --name=DD" + str(i) + " " + DOCKER_IMAGE + ";"
    CMD+="docker exec -d DD"+str(i)+" java -cp \"${CP}\" DataNode.DataNode " + str(i) +";"

print CMD
print
os.system(CMD);

STOP_CMD = "export PATH=$PATH:/usr/local/bin;eval $(docker-machine env docker);"
STOP_CMD += "export PATH=$PATH:/usr/local/bin;"
STOP_CMD += "eval $(docker-machine env docker);"
STOP_CMD += "docker kill $(docker ps -a -q);"
STOP_CMD += "docker rm $(docker ps -a -q);"
STOP_CMD += "docker network rm my-net"

def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        os.system(STOP_CMD)
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
print('Press Ctrl+C to exit')
signal.pause()