package NameNode;

import java.util.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.lang.*;
import java.io.*;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.TimerTask;
import java.util.Timer;
import com.distributedsystems.HDFS.*;
import com.distributedsystems.namenode.NameNodeProto.*;

public class NameNode implements INameNode {

    private static int RMIPORT;
    // in milliseconds
    private static int ALIVE_THREAD_TIME;
    // to be changed in DataNodeAliveThread. Kept here for bookkeeping
    private static int DECLARE_DEAD_TIME;
    private static String NAMENODE_FILE;
    private static int replication_factor;
    private static int BlockNum;

    private static NameNodeStorage.Builder persistant_data;

    public static Registry registry;
    // Handle : filename
    public static HashMap<Integer, String> HandleToFile;
    public static int handleno;
    // filename : [<block ids>]
    public static HashMap<String, Vector<Integer>> FileBlocks;
    // DatanodeID : Timestamp
    public static LinkedHashMap<Integer, Long> AliveDataNodes;
    // DatanodeID : Blocks
    public static HashMap<Integer, Vector<Integer>> DataNodeBlocks;
    // Block : Datanodes ID
    public static HashMap<Integer, LinkedHashSet<Integer>> BlocksDataNode;
    // DataNodeID : Datanode location
    public static HashMap<Integer, DataNodeLocation> DataNodeLoc;
    public static HashSet<String> ClosedFiles;

    // constructor
    public NameNode() {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "hdfs.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }

            prop.load(input);

            this.RMIPORT = Integer.parseInt(prop.getProperty("namenode_port"));
            this.ALIVE_THREAD_TIME = Integer.parseInt(prop.getProperty("alive_thread_time"));
            this.DECLARE_DEAD_TIME = Integer.parseInt(prop.getProperty("declare_dead_time"));
            this.NAMENODE_FILE = prop.getProperty("namenode_file");
            this.replication_factor = Integer.parseInt(prop.getProperty("replication_factor"));

            this.HandleToFile = new HashMap<Integer, String>();
            this.FileBlocks = new HashMap<String, Vector<Integer>>();
            this.AliveDataNodes = new LinkedHashMap<Integer, Long>();
            this.DataNodeBlocks = new HashMap<Integer, Vector<Integer>>();
            this.DataNodeLoc = new HashMap<Integer, DataNodeLocation>();
            this.BlocksDataNode = new HashMap<Integer, LinkedHashSet<Integer>>();
            this.ClosedFiles = new HashSet<String>();
            this.persistant_data = NameNodeStorage.newBuilder();

            // If restarting use the previous state
            File file_to_write = new File(this.NAMENODE_FILE);
            if (file_to_write.exists() && !file_to_write.isDirectory()) {
                InputStream inputstream = new FileInputStream(this.NAMENODE_FILE);
                DataInputStream datainputstream = new DataInputStream(inputstream);
                int total_bytes = inputstream.available();
                byte[] read_bytes = new byte[total_bytes];
                datainputstream.read(read_bytes);
                inputstream.close();
                NameNodeStorage namenode_store = NameNodeStorage.parseFrom(read_bytes);
                this.persistant_data = namenode_store.toBuilder();
                this.BlockNum = namenode_store.getLatestBlockno();
                this.handleno = namenode_store.getLatestHadleno();
                for (NameNodeStorage.FileInfo i : namenode_store.getFileinfoList()) {
                    this.ClosedFiles.add(i.getFileName());
                    this.HandleToFile.put(i.getHandle(), i.getFileName());
                    Vector<Integer> blk_list = new Vector<Integer>();
                    for (Integer j : i.getBlockidList()) {
                        blk_list.add(j);
                    }
                    this.FileBlocks.put(i.getFileName(), blk_list);
                }
            } else {
                this.BlockNum = 0;
                this.handleno = 0;
            }

        } catch (Exception e) {
            System.err.println("Problem running NameNode constructor: " + e.getMessage());
            e.printStackTrace();

        }
    }

    synchronized public byte[] openFile(byte[] inp) throws RemoteException {
        OpenFileResponse.Builder response = OpenFileResponse.newBuilder();
        try {
            OpenFileRequest openreq = OpenFileRequest.parseFrom(inp);
            String filename = openreq.getFileName();
            boolean forRead = openreq.getForRead();
            if (!forRead) {
                //openfile for write

                int handle = this.handleno++;

                HandleToFile.put(handle, filename);
                FileBlocks.put(filename, new Vector<Integer>());

                response.setStatus(0);
                response.setHandle(handle);
            } else if (FileBlocks.get(filename) == null) {
                response.setStatus(1);
            } else {
                response.setStatus(0);
                Vector<Integer> blocknums = FileBlocks.get(filename);
                response.addAllBlockNums(blocknums);
            }
        } catch (Exception e) {
            System.err.println("Problem running OpenFile. " + e.getMessage());
            e.printStackTrace();
            // Catched Exception Inform Client
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] closeFile(byte[] inp) throws RemoteException {
        CloseFileResponse.Builder response = CloseFileResponse.newBuilder();
        try {
            CloseFileRequest clfr = CloseFileRequest.parseFrom(inp);
            int filehandle = clfr.getHandle();
            if (HandleToFile.get(filehandle) == null) {
                // ReadFile doesnt call closefile
                response.setStatus(1);
            } else {

                //write data to disk from FileBlocks
                NameNodeStorage.FileInfo.Builder file_data = NameNodeStorage.FileInfo.newBuilder();

                String filename = HandleToFile.get(filehandle);

                file_data.setFileName(filename);
                file_data.setHandle(filehandle);
                file_data.addAllBlockid(FileBlocks.get(filename));
                this.persistant_data.setLatestHadleno(this.handleno);
                this.persistant_data.setLatestBlockno(this.BlockNum);
                this.persistant_data.addFileinfo(file_data);
                byte[] data_to_store = persistant_data.build().toByteArray();
                // overwrite current file
                FileOutputStream fos = new FileOutputStream(this.NAMENODE_FILE);
                fos.write(data_to_store);
                fos.close();
                response.setStatus(0);
                this.ClosedFiles.add(filename);
            }

        } catch (Exception e) {
            System.err.println("Problem running CloseFile. " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] getBlockLocations(byte[] inp) throws RemoteException {
        BlockLocationResponse.Builder response = BlockLocationResponse.newBuilder();
        try {
            BlockLocationRequest blr = BlockLocationRequest.parseFrom(inp);
            response.setStatus(1);
            outerloop:

            for (int i : blr.getBlockNumsList()) {
                BlockLocations.Builder blk_loc = BlockLocations.newBuilder();
                // set block number
                blk_loc.setBlockNumber(i);
                // add datanode location of given block number to blocklocation
                LinkedHashSet<Integer> tempset = BlocksDataNode.get(i);
                if (tempset == null) {
                    continue;
                }

                for (int j : tempset) {
                    DataNodeLocation temploc = DataNodeLoc.get(j);
                    if (temploc == null) {
                        // Node might be dead, didnt remove extra info stored coz always checked
                        tempset.remove(j);
                    } else {
                        response.setStatus(0);
                        // add datanode location to current blocklocation
                        blk_loc.addLocations(temploc);
                    }

                }

                // add block location to response
                response.addBlockLocations(blk_loc);
            }

        } catch (Exception e) {
            System.err.println("Problem running Get Block Locations. " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] assignBlock(byte[] inp) throws RemoteException {
        // Assuming client will call this function to get one block
        AssignBlockResponse.Builder response = AssignBlockResponse.newBuilder();
        try {
            AssignBlockRequest request = AssignBlockRequest.parseFrom(inp);
            int handle = request.getHandle();

            this.BlockNum++;
            BlockLocations.Builder blk_loc = BlockLocations.newBuilder();
            //Increment block number and use it as the assigned block
            blk_loc.setBlockNumber(this.BlockNum);
            //Choose based on replication factor from list of alive datanodes
            Object[] alive_datanodes = this.AliveDataNodes.keySet().toArray();
            response.setStatus(0);
            HashSet<Integer> random_nodes = new HashSet<Integer>();
            // Select random data nodes based on replication factor and add to response
            //If alive nodes less than replication factor return error to stop infinite loop below
            if (alive_datanodes.length < this.replication_factor) {

                System.err.println("replication");
                response.setStatus(3);
                byte[] ser_response = response.build().toByteArray();
                return ser_response;
            }
            List<Integer> alive_datanodes_list = new ArrayList<Integer>(AliveDataNodes.keySet());
            for (int i = 0; i < this.replication_factor; i++) {
                int cand_index = (int) (Math.random() * (alive_datanodes_list.size()));
                int cand = alive_datanodes_list.get(cand_index);
                while (random_nodes.contains(cand)) {
                    // find new candidate data node if already used for the same block
                    cand_index = (int) (Math.random() * (alive_datanodes_list.size()));
                    cand = alive_datanodes_list.get(cand_index);
                }
                random_nodes.add(cand);
                if (this.DataNodeLoc.get(cand) == null) {
                    response.setStatus(1);
                    break;
                } else {
                    blk_loc.addLocations(this.DataNodeLoc.get(cand));
                }
            }

            response.setNewBlock(blk_loc);
            this.FileBlocks.get(this.HandleToFile.get(handle)).add(this.BlockNum);
        } catch (Exception e) {
            System.err.println("Problem running Assign Block Locations. " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] list(byte[] inp) throws RemoteException {
        ListFilesResponse.Builder response = ListFilesResponse.newBuilder();
        try {
            // input not used since no directory
            response.setStatus(0);
            response.addAllFileNames(this.ClosedFiles);
        } catch (Exception e) {
            System.err.println("Problem running List. " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] blockReport(byte[] inp) throws RemoteException {
        BlockReportResponse.Builder response = BlockReportResponse.newBuilder();
        try {
            BlockReportRequest brr = BlockReportRequest.parseFrom(inp);
            int datanode_id = brr.getId();
            Vector<Integer> blocks = new Vector<Integer>();
            for (int i : brr.getBlockNumbersList()) {
                if (this.BlocksDataNode.containsKey(i) != true) {
                    LinkedHashSet<Integer> temp = new LinkedHashSet<Integer>();
                    temp.add(datanode_id);
                    this.BlocksDataNode.put(i, temp);
                    // block number was not in the list
                    response.addStatus(1);
                } else {
                    // Add datanode_id to LinkedHashSet
                    LinkedHashSet<Integer> datanode_ids = this.BlocksDataNode.get(i);
                    datanode_ids.add(datanode_id);
                    this.BlocksDataNode.put(i, datanode_ids);
                    // block number was in the list
                    response.addStatus(0);
                }
                blocks.add(i);
            }

            // Store info about node blocks
            this.DataNodeBlocks.put(datanode_id, blocks);
            // Get location and store it in map            
            this.DataNodeLoc.put(datanode_id, brr.getLocation());

        } catch (Exception e) {
            System.err.println("Problem running Block Report. " + e.getMessage());
            e.printStackTrace();
            // Clear entire status and add one status with exception
            response.clearStatus();
            response.addStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    synchronized public byte[] heartBeat(byte[] inp) throws RemoteException {
        HeartBeatResponse.Builder response = HeartBeatResponse.newBuilder();
        try {
            HeartBeatRequest heartbr = HeartBeatRequest.parseFrom(inp);
            int datanode_id = heartbr.getId();

            this.AliveDataNodes.put(datanode_id, System.currentTimeMillis());

            response.setStatus(0);
        } catch (Exception e) {
            System.err.println("Problem running HeartBeat. " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        byte[] ser_response = response.build().toByteArray();
        return ser_response;
    }

    private void DataNodeAliveThread() {
        Timer hdfs_timer = new Timer();
        hdfs_timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            // To synchronize with data structures
            synchronized public void run() {
                try {
                    for (int i : AliveDataNodes.keySet()) {
                        long time_diff = System.currentTimeMillis() - AliveDataNodes.get(i);
                        if (time_diff > DECLARE_DEAD_TIME) {
                            // Update data structures
                            AliveDataNodes.remove(i);
                            DataNodeBlocks.remove(i);
                            DataNodeLoc.remove(i);
                        }
                    }
                } catch (Exception e) {
                    System.err.println("Problem running DataNodeAliveThread " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }, 0, this.ALIVE_THREAD_TIME);
    }

    public static void main(String args[]) {
        System.err.println("Setting up NameNode.");

        try {
            NameNode namenode_server = new NameNode();
            namenode_server.registry = LocateRegistry.createRegistry(namenode_server.RMIPORT);
            INameNode namenode_stub = (INameNode) UnicastRemoteObject.exportObject(namenode_server, 0);
            namenode_server.registry.bind("NameNode", namenode_stub);
            System.err.println("NameNode Setup Complete.");
            namenode_server.DataNodeAliveThread();

        } catch (Exception e) {
            System.err.println("NameNode Server Exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
