package NameNode;

import java.lang.*;
import java.util.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class Clean {

    private Clean() {
    }

    public static void main(String[] args) {
        try {
            LocateRegistry.getRegistry(1099).unbind("NameNode");
        } catch (Exception e) {

            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
