/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JobClient;

import JobTracker.IJobTracker;
import com.MapReducer.MR.*;
import java.io.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

public class JobClient {
    
    private static String jobtracker_ip;
    private static int jobtracker_port;
    private static IJobTracker jobtracker_stub;
    private static byte[] job_status_request;
    
    public JobClient() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "mapreduce.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }

            prop.load(input);
            this.jobtracker_ip = prop.getProperty("jobtracker_ip");
            this.jobtracker_port = Integer.parseInt(prop.getProperty("jobtracker_port"));
        } catch (Exception e) {
            System.err.println("Problem initialising JobClient " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private int submit(String[] args) {
        
        int numReducers = 0;
        try {
            numReducers = Integer.parseInt(args[4]);
        }
        catch (NumberFormatException e) {
            System.err.println("Argument" + args[4] + " must be an integer.");
            return 1;
        }
        
        JobSubmitRequest.Builder request_builder = JobSubmitRequest.newBuilder();
        request_builder.setMapName(args[0]);
        request_builder.setReducerName(args[1]);
        request_builder.setInputFile(args[2]);
        request_builder.setOutputFile(args[3]);
        request_builder.setNumReduceTasks(numReducers);
        
        byte[] request = request_builder.build().toByteArray();
        JobSubmitResponse response;
        
        try {
            byte[] response_bytes = this.jobtracker_stub.jobSubmit(request);
            response = JobSubmitResponse.parseFrom(response_bytes);
            if(response.getStatus() != 0)
                throw new Exception("Response Status != 0"); 
        }
        catch (Exception e) {
            System.err.println("Error Making RPC call (jobSubmit): " + e.toString());
            e.printStackTrace();
            return 2;
        }
        
        int job_id = response.getJobId();
        
        JobStatusRequest.Builder status_request = JobStatusRequest.newBuilder();
        status_request.setJobId(job_id);
        this.job_status_request = status_request.build().toByteArray();
        
        return 0;
    }
    
    private boolean getStatus() {
        JobStatusResponse response;
        try {
            byte[] response_bytes = this.jobtracker_stub.getJobStatus(this.job_status_request);
            response = JobStatusResponse.parseFrom(response_bytes);
            if(response.getStatus() != 0)
                throw new Exception("Response Status != 0"); 
        }
        catch (Exception e) {
            System.err.println("Error making RPC call (jobStatus): " + e.toString());
            e.printStackTrace();
            return false;
        }
        
        System.out.print("Status: MapTasksStarted=");
        System.out.print(response.getNumMapTasksStarted() + "/" + response.getTotalMapTasks());
        System.out.print(" ReduceTasksStarted=");
        System.out.print(response.getNumReduceTasksStarted() + "/" + response.getTotalReduceTasks());
        System.out.print(" JobDone=" + response.getJobDone());
        System.out.println();
        
        return response.getJobDone();
    }
    
    public static void main(String[] args) throws IOException {
        try {
            JobClient client = new JobClient();
            Registry registry = LocateRegistry.getRegistry(client.jobtracker_ip, client.jobtracker_port);
            client.jobtracker_stub = (IJobTracker) registry.lookup("JobTracker");
            
            if (args.length != 5) {
                System.err.println("Usage: JobClient <mapName> <reducerName> <inputFile in HDFS> <outputFile in HDFS> <numReducers>");
                System.exit(1);
            }
            
            int ret = client.submit(args);
            if(ret != 0)
                throw new Exception("submit failed!");
            
            while(client.getStatus() == false) {
                Thread.sleep(200);
            }
        }
        catch (Exception e) {
            System.err.println("Error Starting Job: " + e.toString());
            e.printStackTrace();
        }
    }
}
