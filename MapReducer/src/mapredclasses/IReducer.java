package mapredclasses;

public interface IReducer {
	public String reduce(String inp);
}
