package JobTracker;

import com.MapReducer.MR.*;
import com.distributedsystems.HDFS.BlockLocationRequest;
import com.distributedsystems.HDFS.BlockLocationResponse;
import com.distributedsystems.HDFS.BlockLocations;
import com.distributedsystems.HDFS.OpenFileRequest;
import com.distributedsystems.HDFS.OpenFileResponse;

import java.io.*;
import java.rmi.registry.*;
import java.util.*;
import hdfs.*;
import java.rmi.server.*;

public class JobTracker implements IJobTracker {

    private int jobid_number;
    private int taskid_number;

    private String namenode_ip;
    private int namenode_port;
    private INameNode namenode_stub;

    private int RMIPORT;
    private Registry registry;
    private IJobTracker exported_stub;

    public class Job {

        public int jobid;
        public String mapname, reducename, inputfile, outputfile;
        public int num_reduce_tasks;
        public HashSet<Integer> MapTasks, ReduceTasks;
        public HashSet<Integer> MapTasksStarted, ReduceTasksStarted;
        public HashSet<Integer> MapTasksCompleted, ReduceTasksCompleted;
        public HashMap<Integer, BlockLocations> MapTaskToLocation;
        public Vector<String> MapOutputFiles;
        public HashMap<Integer, Vector<String>> ReduceIDToMapOutputFiles;

        public Job(int jid, String mname, String rname, String ifile,
                String ofile, int rtasks) {
            this.jobid = jid;
            this.mapname = mname;
            this.reducename = rname;
            this.inputfile = ifile;
            this.outputfile = ofile;
            this.num_reduce_tasks = rtasks;
        }

        public boolean is_done() {
            return this.MapTasks.equals(this.MapTasksCompleted)
                    && this.ReduceTasks.equals(this.ReduceTasksCompleted);
        }

        public boolean map_is_done() {
            return this.MapTasks.equals(this.MapTasksCompleted);
        }
    }

    private HashMap<Integer, Job> JobIDToJob;
    private HashMap<Integer, Integer> TaskIDToJobID;
    private Queue<Integer> MapTasksQueue, ReduceTasksQueue;

    public JobTracker() {
        Properties prop = new Properties();
        InputStream input;
        try {
            String filename = "jobtracker.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }
            prop.load(input);
            this.namenode_ip = prop.getProperty("namenode_ip");
            this.namenode_port = Integer.parseInt(prop.getProperty("namenode_port"));

            this.jobid_number = 0;
            this.taskid_number = 0;

            this.TaskIDToJobID = new HashMap<>();
            this.JobIDToJob = new HashMap<>();
            this.MapTasksQueue = (Queue<Integer>) new LinkedList<Integer>();
            this.ReduceTasksQueue = (Queue<Integer>) new LinkedList<Integer>();

            this.registry = LocateRegistry.createRegistry(this.RMIPORT);
            this.exported_stub = (IJobTracker) UnicastRemoteObject.
                    exportObject(this, 0);
            this.registry.bind("JobTracker", exported_stub);

            Registry namenode_registry = LocateRegistry.
                    getRegistry(this.namenode_ip, this.namenode_port);
            this.namenode_stub = (INameNode) namenode_registry.
                    lookup("NameNode");
        } catch (Exception e) {
            System.err.println("Problem in JobTracker Initialization " + 
                    e.getMessage());
            e.printStackTrace();
        }
    }

    synchronized private void processCompletions(HeartBeatRequest request) {
        for (MapTaskStatus status : request.getMapStatusList()) {
            if (status.getTaskCompleted()) {
                Job job = this.JobIDToJob.get(status.getJobId());
                
                job.MapTasksCompleted.add(status.getTaskId());
                job.MapOutputFiles.add(status.getMapOutputFile());
                if(job.map_is_done()) {
                    for(int i=0; i<job.num_reduce_tasks; i++) 
                        job.ReduceIDToMapOutputFiles.put(this.taskid_number++,
                                new Vector<>());
                    
                    // equally distribute reduce tasks
                    Vector<Integer> keys = (Vector<Integer>) 
                            job.ReduceIDToMapOutputFiles.keySet();
                    for(int i=0; i<job.MapOutputFiles.size(); i++) {
                        job.ReduceIDToMapOutputFiles.
                                get(keys.elementAt(i%job.num_reduce_tasks)).
                                add(job.MapOutputFiles.elementAt(i));
                    }
                    
                    for(Integer i : keys)
                        this.ReduceTasksQueue.add(i);
                }
            }
        }
        for (ReduceTaskStatus status : request.getReduceStatusList()) {
            if (status.getTaskCompleted()) {
                this.JobIDToJob.get(status.getJobId()).ReduceTasksCompleted.
                        add(status.getTaskId());
            }
        }
    }

    synchronized public byte[] jobSubmit(byte[] inp) {
        JobSubmitResponse.Builder response = JobSubmitResponse.newBuilder();
        try {
            JobSubmitRequest request = JobSubmitRequest.parseFrom(inp);

            Job job = new Job(this.jobid_number,
                    request.getMapName(),
                    request.getReducerName(),
                    request.getInputFile(),
                    request.getOutputFile(),
                    request.getNumReduceTasks()
            );

            this.jobid_number = this.jobid_number + 1;

            this.JobIDToJob.put(job.jobid, job);

            response.setJobId(job.jobid);

            // Sending openfile request to namenode
            OpenFileRequest.Builder ofr = OpenFileRequest.newBuilder();
            ofr.setForRead(true);
            ofr.setFileName(request.getInputFile());

            OpenFileResponse ofr_resp
                    = OpenFileResponse.parseFrom(
                            this.namenode_stub.openFile(
                                    ofr.build().toByteArray()));

            if (ofr_resp.getStatus() != 0) {
                throw new Exception("Error in response when openfile "
                        + "request for reading was sent.");
            }

            int handle = ofr_resp.getHandle();
            BlockLocationRequest.Builder blr = BlockLocationRequest.newBuilder();
            blr.addAllBlockNums(ofr_resp.getBlockNumsList());
            BlockLocationResponse block_loc_res
                    = BlockLocationResponse.parseFrom(
                            this.namenode_stub.getBlockLocations(
                                    blr.build().toByteArray()));

            if (block_loc_res.getStatus() != 0) {
                throw new Exception("Error in response when openfile "
                        + "request for reading was sent.");
            }
            // put it to apt datastructure

            for (BlockLocations i : block_loc_res.getBlockLocationsList()) {
                job.MapTasks.add(this.taskid_number);
                this.TaskIDToJobID.put(this.taskid_number, job.jobid);
                job.MapTaskToLocation.put(this.taskid_number, i);
                this.MapTasksQueue.add(this.taskid_number);
                this.taskid_number = this.taskid_number + 1;
            }

            response.setStatus(0);
        } catch (Exception e) {
            System.err.println("Problem in jobSubmit " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }

        return response.build().toByteArray();
    }

    synchronized public byte[] getJobStatus(byte[] inp) {
        JobStatusResponse.Builder response = JobStatusResponse.newBuilder();
        try {
            JobStatusRequest request = JobStatusRequest.parseFrom(inp);
            Job job = this.JobIDToJob.get(request.getJobId());

            response.setTotalMapTasks(job.MapTasks.size());
            response.setTotalReduceTasks(job.num_reduce_tasks);

            response.setNumMapTasksStarted(job.MapTasksStarted.size());
            response.setNumReduceTasksStarted(job.ReduceTasksStarted.size());

            response.setJobDone(job.is_done());

            response.setStatus(0);
        } catch (Exception e) {
            System.err.println("Problem in getJobStatus " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        return response.build().toByteArray();
    }

    synchronized public byte[] heartBeat(byte[] inp) {
        HeartBeatResponse.Builder response = HeartBeatResponse.newBuilder();
        try {
            HeartBeatRequest request = HeartBeatRequest.parseFrom(inp);

            //process status
            processCompletions(request);
            
            //submit new map tasks
            int num_map_tasks = request.getNumMapSlotsFree();
            while(MapTasksQueue.isEmpty() == false
                    && num_map_tasks>0) {
                int task_id = MapTasksQueue.remove();
                Job job = this.JobIDToJob.get(this.TaskIDToJobID.get(task_id));
                
                MapTaskInfo.Builder task_info = MapTaskInfo.newBuilder();
                task_info.setJobId(job.jobid);
                task_info.setTaskId(task_id);
                task_info.setMapName(job.mapname);
                task_info.addInputBlocks(job.MapTaskToLocation.get(task_id));
                
                response.addMapTasks(task_info);
                
                num_map_tasks--;
            }

            //submit new reduce tasks
            int num_reduce_tasks = request.getNumReduceSlotsFree();
            while(ReduceTasksQueue.isEmpty() == false
                    && num_reduce_tasks>0) {
                int task_id = ReduceTasksQueue.remove();
                Job job = this.JobIDToJob.get(this.TaskIDToJobID.get(task_id));
                
                ReducerTaskInfo.Builder task_info = ReducerTaskInfo.newBuilder();
                task_info.setJobId(job.jobid);
                task_info.setTaskId(task_id);
                task_info.setReducerName(job.reducename);
                task_info.setOutputFile(job.outputfile);
                task_info.addAllMapOutputFiles(
                        job.ReduceIDToMapOutputFiles.get(task_id));
                
                response.addReduceTasks(task_info);
                
                num_reduce_tasks--;
            }
            
            response.setStatus(0);
        } catch (Exception e) {
            System.err.println("Problem in heartBeat " + e.getMessage());
            e.printStackTrace();
            response.setStatus(2);
        }
        return response.build().toByteArray();
    }

    public static void main(String[] args) throws IOException {
        try {
            JobTracker jt = new JobTracker();
        } catch (Exception e) {
            System.err.println("Error Starting Server: " + e.toString());
            e.printStackTrace();
        }
    }
}
