package TaskTracker;

import JobTracker.IJobTracker;
import com.MapReducer.MR.*;
import com.distributedsystems.HDFS;
import com.distributedsystems.HDFS.BlockLocations;
import com.distributedsystems.HDFS.DataNodeLocation;
import com.distributedsystems.HDFS.ReadBlockRequest;
import com.distributedsystems.HDFS.ReadBlockResponse;
import com.distributedsystems.HDFS.WriteBlockRequest;
import com.distributedsystems.HDFS.WriteBlockResponse;
import com.google.protobuf.ByteString;
import java.io.*;
import java.rmi.registry.*;
import java.util.*;
import hdfs.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.concurrent.BlockingQueue;
import mapredclasses.*;

public class TaskTracker {

    String tasktracker_ip;
    int tasktracker_port;
    int tt_id;
    String tt_ip;
    IJobTracker jobtracker_stub;
    int mapslots;
    int reduceslots;
    private String namenode_ip;
    private int namenode_port;
    private INameNode namenode_stub;
    MapTaskStatus myMapTaskStatus;
    ReduceTaskStatus myReduceTaskStatus;
    public static HashMap<Integer, MapThread> MapProcessingQueue = new HashMap<Integer, MapThread>();
    public static HashMap<Integer, MapThread> MapCompleteQueue = new HashMap<Integer, MapThread>();
    public static HashMap<Integer, ReduceThread> ReduceProcessingQueue = new HashMap<Integer, ReduceThread>();
    public static HashMap<Integer, ReduceThread> ReduceCompleteQueue = new HashMap<Integer, ReduceThread>();

    public TaskTracker() {

        Properties prop = new Properties();
        InputStream input = null;
        try {
            String filename = "tasktracker.config";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                throw new ExceptionInInitializerError();
            }
            prop.load(input);
            this.tasktracker_ip = prop.getProperty("tasktracker_ip");
            this.tasktracker_port = Integer.parseInt(prop.getProperty("tasktracker_port"));
            this.tt_id = Integer.parseInt(prop.getProperty("tasktracker_id"));
            this.tt_ip = prop.getProperty("tasktracker_ip");
            this.mapslots = Integer.parseInt(prop.getProperty("mapslots"));
            this.reduceslots = Integer.parseInt(prop.getProperty("reduceslots"));
            this.namenode_ip = prop.getProperty("namenode_ip");
            this.namenode_port = Integer.parseInt(prop.getProperty("namenode_port"));
            Registry namenode_registry = LocateRegistry.
                    getRegistry(this.namenode_ip, this.namenode_port);
            this.namenode_stub = (INameNode) namenode_registry.
                    lookup("NameNode");
        } catch (Exception e) {
            System.err.println("Problem in JobTracker Initialization " + e.getMessage());
            e.printStackTrace();
        }

    }

    void write_string_to_hdfs(String filename, String string_to_write) {
        try {
            HDFS.OpenFileRequest.Builder open_req = HDFS.OpenFileRequest.newBuilder();
            open_req.setFileName(filename);
            open_req.setForRead(false);
            HDFS.OpenFileResponse response = HDFS.OpenFileResponse
                    .parseFrom(this.namenode_stub
                            .openFile(open_req.build().toByteArray()));

            if (response.getStatus() != 0) {
                System.err.println("Error in response when openfile request for writing was sent.");
            } else {
                int handle = response.getHandle();
                // No need to rebuild since same handle is used for consecutive calls
                HDFS.AssignBlockRequest.Builder assign_blk_req = HDFS.AssignBlockRequest.newBuilder();
                assign_blk_req.setHandle(handle);

                HDFS.AssignBlockResponse ass_resp = HDFS.AssignBlockResponse
                        .parseFrom(this.namenode_stub
                                .assignBlock(assign_blk_req.build()
                                        .toByteArray()));
                if (ass_resp.getStatus() != 0) {
                    System.err.println("Error in response from Assign Block request.");
                } else {
                    // get the assigned block
                    BlockLocations assigned_block = ass_resp.getNewBlock();
                    // send write request to datanode with the assigned block
                    int block_number = assigned_block.getBlockNumber();
                    if (assigned_block.getLocationsCount() == 0) {
                        System.err.println("No data node location to write block");
                    } else {
                        String ass_ip = assigned_block.getLocations(0).getIp();
                        int ass_port = assigned_block.getLocations(0).getPort();
                        Vector<DataNodeLocation> ass_blk_list = new Vector<DataNodeLocation>();
                        for (DataNodeLocation i : assigned_block.getLocationsList()) {
                            ass_blk_list.add(i);
                        }
                        ass_blk_list.remove(0);
                        BlockLocations.Builder updated_ass_blk = BlockLocations.newBuilder();
                        // removed block location to which request is being sent to prevent rewriting during cascade phase
                        updated_ass_blk.setBlockNumber(block_number);
                        updated_ass_blk.addAllLocations(ass_blk_list);
                        Registry datanode_registry = LocateRegistry.getRegistry(ass_ip, ass_port);
                        IDataNode datanode_stub = (IDataNode) datanode_registry.lookup("DataNode");
                        WriteBlockRequest.Builder write_block_req = WriteBlockRequest.newBuilder();
                        write_block_req.setData(ByteString.copyFrom(string_to_write.getBytes()));
                        write_block_req.setBlockInfo(updated_ass_blk);
                        WriteBlockResponse write_response = WriteBlockResponse
                                .parseFrom(datanode_stub.writeBlock(write_block_req.build().toByteArray()));
                        if (write_response.getStatus() != 0) {
                            System.err.println("Error in WriteBlockResponse");
                        } else {
                            System.err.println("Block written successfully");
                        }
                        HDFS.CloseFileRequest.Builder close_file_req = HDFS.CloseFileRequest.newBuilder();
                        close_file_req.setHandle(handle);
                        this.namenode_stub.closeFile(close_file_req.build().toByteArray());
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Problem Saving file " + e.getMessage());
            e.printStackTrace();
        }
    }

    static class HeartBeatThread extends Thread {

        private HeartBeatRequest.Builder request;
        private int tt_id;
        private String tt_ip;
        private IJobTracker stub_;
        private int HeartBeatDelay;
        private static TaskTracker parent;

        public HeartBeatThread(TaskTracker caller) {
            this.parent = caller;
        }

        public void run() {
            try {
                while (true) {
                    request = HeartBeatRequest.newBuilder();
                    request.setTaskTrackerId(this.parent.tt_id);
                    //request.setTaskTrackerIp(this.parent.tt_ip);
                    request.setNumMapSlotsFree(this.parent.mapslots);
                    request.setNumReduceSlotsFree(this.parent.reduceslots);
                    for (int i : MapCompleteQueue.keySet()) {
                        MapTaskStatus.Builder mts = MapTaskStatus.newBuilder();
                        MapThread mt = MapCompleteQueue.get(i);
                        mts.setJobId(mt.jobid);
                        mts.setTaskId(mt.taskid);
                        mts.setMapOutputFile(mt.mapoutputfile);
                        mts.setTaskCompleted(true);
                        request.addMapStatus(mts);
                    }
                    for (int i : ReduceCompleteQueue.keySet()) {
                        ReduceTaskStatus.Builder rts = ReduceTaskStatus.newBuilder();
                        ReduceThread rt = ReduceCompleteQueue.get(i);
                        rts.setJobId(rt.jobid);
                        rts.setTaskId(rt.taskid);
                        rts.setTaskCompleted(true);
                        request.addReduceStatus(rts);
                    }
                    byte[] heartbeat_response = this.parent.jobtracker_stub.heartBeat(request.build().toByteArray());
                    HeartBeatResponse response = HeartBeatResponse.parseFrom(heartbeat_response);
                    if (response.getStatus() != 0) {
                        System.err.println("Error in HeartBeat Response");
                    } else {
                        if (response.getMapTasksCount() > 0) {
                            for (MapTaskInfo i : response.getMapTasksList()) {
                                MapThread mt = new MapThread(i.getJobId(), i.getTaskId(), i.getMapName(), i.getInputBlocksList(), this.parent);
                                Thread th = new Thread(mt);
                                this.parent.MapProcessingQueue.put(i.getTaskId(), mt);
                                th.start();
                                this.parent.mapslots--;
                            }
                        }
                        if (response.getReduceTasksCount() > 0) {
                            for (ReducerTaskInfo i : response.getReduceTasksList()) {
                                ReduceThread rt = new ReduceThread(i.getJobId(), i.getTaskId(), i.getReducerName(), i.getMapOutputFilesList(), i.getOutputFile(), this.parent);
                                Thread th = new Thread(rt);
                                this.parent.ReduceProcessingQueue.put(i.getTaskId(), rt);
                                th.start();
                                this.parent.reduceslots--;
                            }
                        }
                    }
                    Thread.sleep(this.HeartBeatDelay);
                }
            } catch (Exception e) {
                System.err.println("Error Running HeartBeat Thread: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    public static class MapThread implements Runnable {

        int taskid;
        int jobid;
        String mapper;
        List<BlockLocations> inputblock;
        TaskTracker parent;
        String mapoutputfile;
        int type;

        public MapThread(int job, int task, String mapper, List<BlockLocations> inputblock, TaskTracker tt) throws RemoteException {
            this.jobid = job;
            this.taskid = task;
            this.mapper = mapper;
            this.inputblock = inputblock;
            this.parent = tt;
            this.type = 1;
            this.mapoutputfile = "job_" + job + "_map_" + task;
        }

        public void run() {
            try {
                MapThread mt;
                if ((mt = this.parent.MapProcessingQueue.get(this.taskid)) == null) {
                    System.out.println("Error getting from map.");
                }
                Object mapper_object = Class.forName(this.mapper).newInstance();
                // Only one block location sent
                BlockLocations blk_loc = this.inputblock.get(0);
                int block_no = blk_loc.getBlockNumber();
                List<DataNodeLocation> dnl = blk_loc.getLocationsList();
                String ip = dnl.get(0).getIp();
                int port = dnl.get(0).getPort();
                Registry datanode_registry = LocateRegistry.getRegistry(ip, port);
                IDataNode datanode_stub = (IDataNode) datanode_registry.lookup("DataNode");
                ReadBlockRequest.Builder read_block_req = ReadBlockRequest.newBuilder();
                read_block_req.setBlockNumber(block_no);
                ReadBlockResponse read_block_bres = ReadBlockResponse.parseFrom(datanode_stub.readBlock(read_block_req.build().toByteArray()));
                //Sending entire block as string
                //Assuming the output will be less than block size
                String map_output = ((IMapper) mapper_object).map(read_block_bres.getData().toString());
                // write output to file
                this.parent.write_string_to_hdfs(this.mapoutputfile, map_output);
                if (this.parent.MapProcessingQueue.remove(this.taskid) == null) {
                    System.out.println("Error removing from map.");
                }
                this.parent.MapCompleteQueue.put(this.taskid, mt);
                this.parent.mapslots++;
                System.out.println("Map Thread " + this.taskid + " completed.");
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    public static class ReduceThread implements Runnable {

        int jobid;
        int taskid;
        String reducer;
        List<String> mapoutputfiles;
        String outputfile;
        TaskTracker parent;
        int type;
        String reduceoutputfile;
        String reduce_output;

        public ReduceThread(int job, int task, String reducer,
                List<String> mapOpFile, String outputfile, TaskTracker tt) throws RemoteException {
            this.jobid = job;
            this.taskid = task;
            this.reducer = reducer;
            this.mapoutputfiles = mapOpFile;
            this.outputfile = outputfile;
            this.reduce_output = "";
            this.parent = tt;
            this.type = 2;
            this.reduceoutputfile = outputfile + "_ " + job + "_" + task;
        }

        public void run() {
            try {
                ReduceThread rt;
                if ((rt = this.parent.ReduceProcessingQueue.get(this.taskid)) != null) {
                    System.out.println("Error getting from map.");
                }

                // Process Reduce
                Object reducer_object = Class.forName(this.reducer).newInstance();
                // Reading mapoutput files passing it to reducer and concatinating reducer results in a string to write
                for (String i : mapoutputfiles) {
                    HDFS.OpenFileRequest.Builder open_req = HDFS.OpenFileRequest.newBuilder();
                    open_req.setFileName(i);
                    open_req.setForRead(true);
                    HDFS.OpenFileResponse response = HDFS.OpenFileResponse
                            .parseFrom(this.parent.namenode_stub
                                    .openFile(open_req.build().toByteArray()));
                    if (response.getStatus() != 0) {
                        System.err.println("Error in response when openfile request for reading was sent.");
                    } else {
                        int handle = response.getHandle();
                        HDFS.BlockLocationRequest.Builder blr = HDFS.BlockLocationRequest.newBuilder();
                        blr.addAllBlockNums(response.getBlockNumsList());
                        HDFS.BlockLocationResponse block_loc_res = HDFS.BlockLocationResponse
                                .parseFrom(this.parent.namenode_stub
                                        .getBlockLocations(blr.build().toByteArray()));
                        if (block_loc_res.getStatus() != 0) {
                            System.err.println("Error in response when openfile request for reading was sent.");
                        } else {

                            for (BlockLocations j : block_loc_res.getBlockLocationsList()) {
                                int block_no = j.getBlockNumber();
                                List<DataNodeLocation> dnl = j.getLocationsList();
                                if (dnl.size() == 0) {
                                    System.err.println("Error: No Location found for block number " + block_no);
                                } else {
                                    String ip = dnl.get(0).getIp();
                                    int port = dnl.get(0).getPort();
                                    Registry datanode_registry = LocateRegistry.getRegistry(ip, port);
                                    DataNode.IDataNode datanode_stub = (DataNode.IDataNode) datanode_registry.lookup("DataNode");
                                    ReadBlockRequest.Builder read_block_req = ReadBlockRequest.newBuilder();
                                    read_block_req.setBlockNumber(block_no);
                                    ReadBlockResponse read_block_bres = ReadBlockResponse
                                            .parseFrom(datanode_stub.readBlock(read_block_req
                                                    .build().toByteArray()));
                                    this.reduce_output = this.reduce_output
                                            + ((IReducer) reducer_object)
                                            .reduce(read_block_bres
                                                    .getData()
                                                    .toString());

                                }

                            }
                        }
                    }
                }
                // this.reduceoutput contains concatinated reduce output
                // write output to file
                this.parent.write_string_to_hdfs(this.reduceoutputfile, this.reduce_output);
                if (this.parent.ReduceProcessingQueue.remove(this.taskid) != null) {
                    System.out.println("Error removing from map.");
                }
                this.parent.ReduceCompleteQueue.put(this.taskid, rt);
                System.out.println("Redice Thread " + this.taskid + " completed.");
                this.parent.reduceslots++;
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        try {
            TaskTracker tt = new TaskTracker();
            Registry registry = LocateRegistry.getRegistry(tt.tasktracker_ip, tt.tasktracker_port);
            tt.jobtracker_stub = (IJobTracker) registry.lookup("JobTracker");
        } catch (Exception e) {
            System.err.println("Error Starting Server: " + e.toString());
            e.printStackTrace();
        }
    }

}
